kata13
===================================

## General notes
* Formatting is done with current IDE formatting rules. Other formatting rules can be applied with other IDE or gradle plugin.
* Exceptions, nulls and other special cases are not handled.
* Public fields are used instead of getters/setters where possible.
* All files are taken into account, not only *.java files.
* Test files are named *.jv to avoid compilation by some tools.


## Run instructions
* Set java to version 14. App may work with 11, but this was not tested (note version is specified in build.gradle). Use one of these:
    * `sdk use java <version>`;
    * export JAVA_HOME;
    * update-alternatives;
    * any other ways.


* Running the app:
    * cd to project root ("kata13" dir)
    * Run tests with command like `./gradlew cleanTest test -i`
    * Run option 1. Issue command like `./gradlew run -q --args="<PATH>"`
    * Run option 2. Issue `./gradlew install`, then `cd build/install/kata13/bin/` (exact path may vary), then `./kata13 <PATH>`
    
Run notes:
* `<PATH>` is a relative/absolute path to file/dir.
* Don't run the app for current dir (.) if you have kata13 logs, because the app will try to parse logs and freeze.
