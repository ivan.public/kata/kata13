package com.ivan.kata.kata13;

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Deque;
import java.util.LinkedList;

/**
 * Visitor which calculates lines for all files and dirs.
 * 
 * @author Ivan Bondarenko
 *
 */
public class DequeFileVisitor extends SimpleFileVisitor<Path> {
    
    private int level;
    
    // All files and dirs in occurrence order
    private Deque<PathWrapper> wrappers = new LinkedList<>();
    // Current dirs hierarchy
    private Deque<PathWrapper> dirs = new LinkedList<>();
    
    public Deque<PathWrapper> getWrappers() {
        return wrappers;
    }
    
    @Override
    public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
        PathWrapper wrapper = new PathWrapper(dir, level++);
        wrappers.add(wrapper);
        dirs.add(wrapper);
        return super.preVisitDirectory(dir, attrs);
    }
    
    @Override
    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
        level--;
        PathWrapper wrapper = dirs.removeLast();
        addToParent(wrapper);
        return super.postVisitDirectory(dir, exc);
    }
    
    @Override
    public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
        PathWrapper wrapper = new PathWrapper(file, level);
        wrapper.javaLineCount = new JavaFileParser(file.toFile()).parse();
        wrappers.add(wrapper);
        addToParent(wrapper);
        return super.visitFile(file, attrs);
    }
    
    private void addToParent(PathWrapper wrapper) {
        PathWrapper parentWrapper = dirs.peekLast();
        if (parentWrapper != null) {
            parentWrapper.javaLineCount += wrapper.javaLineCount;
        }
    }
    
}
