package com.ivan.kata.kata13;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Deque;

/**
 * Parses files from a directory tree.
 * 
 * @author Ivan Bondarenko
 *
 */
public class FileTreeParser {
    
    public Deque<PathWrapper> parse(Path path) throws Exception {
        DequeFileVisitor visitor = new DequeFileVisitor();
        Files.walkFileTree(path, visitor);
        return visitor.getWrappers();
    }
    
}
