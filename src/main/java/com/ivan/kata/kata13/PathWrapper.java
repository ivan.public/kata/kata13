package com.ivan.kata.kata13;

import java.nio.file.Path;

import org.apache.commons.lang3.StringUtils;

/**
 * Wrapper around {@link Path} with information we gather.
 * 
 * @author Ivan Bondarenko
 *
 */
public class PathWrapper {
    
    public Path path;
    public int level;
    public int javaLineCount;
    
    public PathWrapper(Path path, int level) {
        this.path = path;
        this.level = level;
    }
    
    @Override
    public String toString() {
        return StringUtils.repeat(' ', level * 2) + path.getFileName() + " : " + javaLineCount;
    }
    
}
