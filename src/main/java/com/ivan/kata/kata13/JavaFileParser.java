package com.ivan.kata.kata13;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * The parser implements kind of state machine. Introducing kind of context will
 * reduce code readability, so this parser is a context for itself. Thus methods
 * mostly do not return any results and do have side-effects.
 * 
 * @author Ivan Bondarenko
 *
 */
public class JavaFileParser {
    
    private static final Logger LOGGER = LogManager.getLogger();
    
    private enum State {
        /** No specific area. */
        NONE,
        /** Quote started. */
        QUOTE_OPENED,
        /** Multiline comment started. */
        COMM_OPENED,
        /** Line comment started. */
        LINE_COMM
    }
    
    private enum Append {
        JUST_MOVE, TILL_ELEMENT, TILL_ELEMENT_END, TILL_LINE_END
    }
    
    private static final String COMM_OPEN = "/*";
    private static final String COMM_CLOSE = "*/";
    private static final String LINE_COMMENT = "//";
    private static final String QUOTE_OPEN = "\"";
    private static final String QUOTE_CLOSE = QUOTE_OPEN;
    private static final Map<String, State> KNOWN_OPENINGS = Map.of(LINE_COMMENT, State.LINE_COMM, QUOTE_OPEN, State.QUOTE_OPENED, COMM_OPEN,
            State.COMM_OPENED);
    
    private String name;
    private Scanner scanner;
    private State state = State.NONE;
    private String line;
    private int javaLineCount;
    private int lineStartOffset;
    private int indexOf;
    private int afterIndexOf;
    private StringBuilder javaLine;
    
    public JavaFileParser(File file) throws FileNotFoundException {
        name = file.getName();
        try {
            scanner = new Scanner(file);
        } catch (FileNotFoundException e) {
            throw e;
        }
    }
    
    public JavaFileParser(String name, String content) {
        this.name = name;
        scanner = new Scanner(content);
    }
    
    public int parse() {
        LOGGER.info("---- Started parsing file: " + name);
        // returns int for usage simplification
        while (scanner.hasNextLine()) {
            line = scanner.nextLine();
            parseFullLine();
        }
        LOGGER.info("---- Java line number is: " + javaLineCount);
        return javaLineCount;
    }
    
    private void parseFullLine() {
        lineStartOffset = 0;
        javaLine = new StringBuilder();
        parseLine();
        checkJavaLine();
    }
    
    /**
     * Parses the line. The method performes state transitions in infinite loop
     * until line can be considered parsed. Code style is targeted to
     * performance (no extra classes, structures etc).
     */
    // This can be rewritten more gracefully
    // if more parse operations are needed
    private void parseLine() {
        while (true) {
            Append append = null;
            switch (state) {
            case NONE:
                if (!findOpening()) {
                    // No opening, the rest is java code
                    append = Append.TILL_LINE_END;
                } else {
                    // Quote itself is appended
                    append = state == State.QUOTE_OPENED ? Append.TILL_ELEMENT_END : Append.TILL_ELEMENT;
                }
                break;
            case LINE_COMM:
                // The rest is a line comment
                state = State.NONE;
                return;
            case COMM_OPENED:
                if (findSubstring(line, COMM_CLOSE)) {
                    state = State.NONE;
                    append = Append.JUST_MOVE;
                    break;
                }
                return;
            case QUOTE_OPENED:
                // No specific state after quotes
                state = State.NONE;
                append = findSubstring(line, QUOTE_CLOSE) ? Append.TILL_ELEMENT_END : Append.TILL_LINE_END;
                break;
            }
            appendJava(append);
            if (append == Append.TILL_LINE_END) {
                return;
            }
        }
    }
    
    private void appendJava(Append append) {
        if (append == Append.TILL_LINE_END) {
            javaLine.append(line.substring(lineStartOffset));
        } else {
            if (append != Append.JUST_MOVE) {
                int tillIndex = append == Append.TILL_ELEMENT ? indexOf : afterIndexOf;
                javaLine.append(line.substring(lineStartOffset, tillIndex));
            }
            lineStartOffset = afterIndexOf;
        }
    }
    
    private void checkJavaLine() {
        String javaLineString = javaLine.toString();
        boolean isJava = !javaLineString.isBlank();
        if (isJava) {
            javaLineCount++;
        }
        
        // Some log info
        LOGGER.info((isJava ? "JAVA" : "oooo") + ": `" + line + "`" + "    ->    `" + javaLineString + "`");
    }
    
    private boolean findOpening() {
        int bestIndexOf = -1;
        int bestAfterIndexOf = -1;
        for (Entry<String, State> e : KNOWN_OPENINGS.entrySet()) {
            findSubstring(line, e.getKey());
            if (indexOf >= 0 && (bestIndexOf < 0 || indexOf < bestIndexOf)) {
                state = e.getValue();
                bestIndexOf = indexOf;
                bestAfterIndexOf = afterIndexOf;
            }
        }
        indexOf = bestIndexOf;
        afterIndexOf = bestAfterIndexOf;
        return indexOf >= 0;
    }
    
    private boolean findSubstring(String str, String substr) {
        indexOf = str.indexOf(substr, lineStartOffset);
        boolean found = indexOf >= 0;
        afterIndexOf = !found ? indexOf : indexOf + substr.length();
        return found;
    }
    
}
