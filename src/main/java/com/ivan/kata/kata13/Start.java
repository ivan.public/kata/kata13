package com.ivan.kata.kata13;

import java.nio.file.Path;

public class Start {
    
    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.err.println("Exactly one argument is expected which is path to file or dir.");
            return;
        }
        
        Path rootPath = Path.of(args[0]);
        for (PathWrapper wrapper : new FileTreeParser().parse(rootPath)) {
            System.out.println(wrapper);
        }
        
    }
    
}
