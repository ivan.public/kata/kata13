package com.ivan.kata.kata13;

import java.io.IOException;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

/**
 * Unit tests for {@link JavaFileParser}.
 * 
 * @author Ivan Bondarenko
 *
 */
public class JavaFileParserTest {
    
    private SoftAssertions softly;
    
    @Test
    public void testFiles() {
        softly = new SoftAssertions();
        
        testFile("File0.jv", 3);
        testFile("dir1/File1.jv", 4);
        testFile("dir1/dir2/File2.jv", 13);
        
        softly.assertAll();
    }
    
    private void testFile(String filename, int expected) {
        String content = null;
        try {
            content = new String(getClass().getClassLoader().getResourceAsStream(filename).readAllBytes());
        } catch (IOException e) {
            softly.fail("Exception.", e);
        }
        JavaFileParser parser = new JavaFileParser(filename, content);
        softly.assertThat(parser.parse()).as("Incorrect line number parsed.").isEqualTo(expected);
    }
    
}
