package com.ivan.kata.kata13;

import java.nio.file.Path;
import java.util.Deque;

import org.assertj.core.api.SoftAssertions;
import org.junit.Test;

/**
 * Unit tests for {@link FileTreeParser}.
 * 
 * @author Ivan Bondarenko
 *
 */
public class FileTreeParserTest {
    
    private FileTreeParser parser = new FileTreeParser();
    private SoftAssertions softly;
    
    @Test
    public void testDir() throws Exception {
        softly = new SoftAssertions();
        Deque<PathWrapper> wrappers = parser.parse(Path.of("src/test/resources/"));
        
        assertWrapper(wrappers, "resources", 20);
        assertWrapper(wrappers, "resources/dir1", 17);
        assertWrapper(wrappers, "resources/dir1/dir2", 13);
        assertWrapper(wrappers, "resources/dir1/dir2/File2.jv", 13);
        assertWrapper(wrappers, "resources/dir1/File1.jv", 4);
        assertWrapper(wrappers, "resources/File0.jv", 3);
        
        softly.assertAll();
    }
    
    @Test
    public void testFile() throws Exception {
        softly = new SoftAssertions();
        Deque<PathWrapper> wrappers = parser.parse(Path.of("src/test/resources/File0.jv"));
        
        assertWrapper(wrappers, "resources/File0.jv", 3);
        
        softly.assertAll();
    }
    
    private void assertWrapper(Deque<PathWrapper> wrappers, String pathname, int expectedLines) {
        for (PathWrapper wrapper : wrappers) {
            if (wrapper.path.endsWith(pathname)) {
                softly.assertThat(wrapper.javaLineCount).isEqualTo(expectedLines);
                return;
            }
        }
        softly.fail("Expected file not found: " + pathname);
    }
    
    
}
