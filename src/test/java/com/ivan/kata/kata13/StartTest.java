package com.ivan.kata.kata13;

import java.io.File;

import org.junit.Test;

public class StartTest {
    
    @Test
    public void testDir() throws Exception {
        System.out.println("PWD: " + new File(".").getAbsolutePath());
        Start.main(new String[] { "src/test/resources/" });
    }
}
